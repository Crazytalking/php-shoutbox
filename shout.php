<?php
session_start();

//Sets the Directory of the Saved Text Files
$DIR = "../PHPShoutbox"; 
//Set how many messages will be displayed
$MESSAGE_COUNT = 25;
//Set the Persons username
$USERNAME = "DemoUsername";
//Set the Flood Delay Timer (in Seconds)
$FLOOD_DELAY = 3; //will need to be set in shoutbox_javascript.js as well
//Set the Timezone
date_default_timezone_set('America/New_York'); 



function getFile($file) //Gets the File Contents and returns it
{
	if (file_exists($file))
	{
		$lines = file($file);
	}
	return $lines;
}

function getLines($file) //Returns the number of lines
{
	return count($file);
}

function ChatFilter($String) //The Chat Filter, Compares each word to each filter word and replaces the necessary ones. Then returns the submitted text with the desired words filtered
{	
	$Filter = array("FilterWord1","FilterWord2"); //Words to Filter Out
	$Filtered = array("[Thats a No-No]"); //Filtered into
	$Str = str_ireplace($Filter, "[Thats a No-No]", $String);
	//$Str = str_ireplace($Filter, $Filtered, $String); //Uncomment and remove line above for individual word replacement
	return $Str;
}


if (!isset($_SESSION['ref_file']))
{
	$_SESSION['ref_file'] = "main_chat.txt";
	$_SESSION['shout_banner'] = "Main";
}

if($_POST)
{
	if (isset($_POST["room"])) //Checks if the Room has been changed
	{
		//ref_file Sets the Text File that the chat will access
		//shout_banner Sets the Banner Text (ex: Demo Shoutbox - Main.)
		
		//This is where it sets which text file to access. You can Manually add Rooms here, but you must set them up elsewhere as well. 
		if ($_POST["room"] == 3) //Room3
		{
			$_SESSION['ref_file'] = "main_chat3.txt";
			$_SESSION['shout_banner'] = "Room3";
		}
		elseif ($_POST["room"] == 2) //Room2
		{
			$_SESSION['ref_file'] = "main_chat2.txt";
			$_SESSION['shout_banner'] = "Room2";
		}
		else //Regular
		{
			$_SESSION['ref_file'] = "main_chat.txt";
			$_SESSION['shout_banner'] = "Main";
		}
	}
	
	if (!file_exists($DIR."/".$_SESSION['ref_file'])) //Checks if the text file exists, if not, it creates it in the directory 
	{
		fopen($DIR."/".$_SESSION['ref_file'], "x");
	}

	$file = htmlentities(strip_tags($DIR."/".$_SESSION['ref_file']), ENT_NOQUOTES, 'UTF-8');
	
	
    //check if its an ajax request, exit if not
    if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
        die();
    }
   
    if(isset($_POST["message"]) &&  strlen($_POST["message"])>0)
    {
        //sanitize user name and message received from chat box
        $username = $USERNAME;
        $message = filter_var(trim($_POST["message"]),FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
		$msg_time = date('H:i:s A M d',time()); //message posted time
		if (strlen($message) > 0 && time()>($_SESSION['message_timer']+$FLOOD_DELAY))
		{
			$_SESSION['message_timer'] = time();
			fwrite(fopen($file,'a'), "<div class='shout_msg'><time>".$msg_time."</time><span class='username'>".$username."</span> <span class='message'>".ChatFilter($message)."</span></div>" . "\n");
		}
		
    }
    elseif($_POST["fetch"]==1) //Retrieve and return The Last Defined Amount of Messages
    {
		$Lines = getFile($file);
		$Count = count($Lines);

+		$MESSAGE_COUNT = ($MESSAGE_COUNT>$Count?$Count:$MESSAGE_COUNT);
		for ($i=($Count-$MESSAGE_COUNT);$i<$Count;$i++)
		{
			echo $Lines[$i];
		}
    }
	elseif (strlen($_POST["message"])<=0) //Checks if the Message Text is Blank
	{
		//Do Nothing
	}
    else
    {
        //output error
        header('HTTP/1.1 500 Are you kiddin me?');
        exit();
    }
}