$(document).ready(function() 
{	
	$('a.room_link').click(function () {//Handles the room clicking
		var roomid = 0;
		var roomtitle = $(this).text();
		roomtitle = roomtitle.replace("["," ");
		roomtitle = roomtitle.replace("]"," ");//Removes the brackets from the room name 
	
		//Checks each room, assigns an ID to it and changes the banner
		//This is one of the locations where you can add rooms. 
		if (roomtitle == "Room3")
		{
			roomid = 3;
			$('.cbanner').text("Room3");
		}
		else if (roomtitle == "Room2")
		{
			roomid = 2;
			$('.cbanner').text("Room2");
		}
		else if(roomtitle == "Main")
		{
			roomid = 1;
			$('.cbanner').text("Main");
		}
		
		$.post('shout.php', {"room":roomid});
	});


	if (typeof localStorage['ascroll'] === 'undefined')
	{
		localStorage['ascroll'] = 1;
		$(".ascroll_btn").text("[A]");
	}
	if (localStorage['header'] == 'block')
	{
		$('.toggle_chat').hide();
		$('.bottom_header').hide();
	}

	//automatically refresh after every 1000 milliseconds.
	load_data = {'fetch':1};
	window.setInterval(function(){
	 $.post('shout.php', load_data,  function(data) {
		$('.message_box').html(data);
		if (localStorage['ascroll'] == 1)
		{
			var scrolltoh = $('.message_box')[0].scrollHeight;
			$('.message_box').scrollTop(scrolltoh);
		}
	 });
	}, 1000);
	
	//method to trigger when user hits enter key
	$("#shout_message").keypress(function(evt) {
		if(evt.which == 13) {
				var iusername = $('#shout_username').val();
				var imessage = $('#shout_message').val();
				post_data = {'username':iusername, 'message':imessage};
				
				
				$(".shout_message").prop('readonly', true);
			   
				//send data to "shout.php" using jQuery $.post()
				$.post('shout.php', post_data, function(data) {
				   
					//append data into messagebox with jQuery fade effect!
					$(data).hide().appendTo('.message_box').fadeIn();

					//keep scrolled to bottom of chat!
					var scrolltoh = $('.message_box')[0].scrollHeight;
					$('.message_box').scrollTop(scrolltoh);
				   
					//reset value of message box
					$('#shout_message').val('');
					
					//Set the flood
					FloodLock();
				   
				}).fail(function(err) {
			   
				//alert HTTP server error
				alert(err.statusText);
				});
			}
	});

	//toggle hide/show shout box
	$(".close_btn").click(function (e) {
		//get CSS display state of .toggle_chat element
		localStorage['header'] = $('.toggle_chat').css('display');
		var toggleState = $('.toggle_chat').css('display');
		
		//toggle show/hide chat box
		$('.toggle_chat').slideToggle();
		$('.bottom_header').slideToggle();
	});
	
	//toggle autoscroll
	$(".ascroll_btn").click(function (e) {
		//check for the scroll options
		if(localStorage['ascroll'] == 1)
		{
			localStorage['ascroll'] = 0;
			$(".ascroll_btn").text("[a]");
		}else{
			localStorage['ascroll'] = 1;
			$(".ascroll_btn").text("[A]");
		}
	});
});

//Setup for the Blood Filter
function FloodLock()
{
	var FLOOD_TIME = 3000;
	var input = document.getElementById("shout_message");
	
	input.disabled = true;
	input.placeholder = "3 Seconds Before Next Message";
	
	setTimeout(function()
	{
		input.disabled = false;
		input.placeholder = "Type Message Hit Enter";
	},FLOOD_TIME);
}