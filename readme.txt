This is a modified version of samwebe's Shoutbox (http://www.sanwebe.com/2013/04/creating-shout-box-facebook-style)
---------------------------------------------------------------

This was modified with my sites needs in mind. So some things like the username input has been removed, and the rooms option is all set up manually. 

--------------------
Differences: 
--------------------
-This uses a text file for storage instead of a SQL Database
-The username input has been removed (Assumes your users are already logged into your site and works off the site's username)
-Includes a word filter
-Option for users to disable autoscrolling
-Multiple Rooms (The rooms are a bit clunky in design, both in how they are handled and you have to add them manually


--------------------
Installing: 
--------------------
-Copy the files into the desired location
-Copy the Shoutbox file contents into your site's source
-Configure your settings (See Configuring below)
-Check Adding Rooms to Add or Remove Rooms

--------------------
Configuring: 
--------------------
Most of the easy configuring can be done in shout.php

$DIR - is the Directory (This is used for the saved chat text files)

$MESSAGE_COUNT - Defines how many messages will remain in the shoutbox before they're no longer visible. 

$USERNAME - The user's name, wherever it is defined (for example, the username saved in their session) 

$FLOOD_DELAY - The amount of time (in seconds) after the user posts before they can post again. (Note: this also needs to be set in shoutbox_javascript.js)


--------------------
Adding Rooms:
--------------------
Adding rooms can be a little tricky, as it needs to be done manually in 3 locations. This is not the best way of going about it, but it was better suited for my site's needs.

shoutbox_javascript.js - Starting at Line 11, Add onto this conditional statement, give the room an ID not in use, and a banner name

shout.php - Starting at Line 55, Add onto this conditional statement. Set the Room ID you used before in the javascript
$_SESSION['ref_file'] is the File the text will be saved to
$_SESSION['shout_banner'] is the Banners title which is displayed. 

Shoutbox - Or the file you've copied the shoutbox into. While still under shoutbox.php, it starts at line 12. Replace Room2 with the name you used above. 
- <a href='#' class='room_link'>[Room2]</a> 






