<?php
session_start();
?>
<html>
<head>
<title>PHP Shoutbox Demo</title>
</head>
<body>
<link rel='stylesheet' type='text/css' href='shoutbox_style.css'/>
<script src='jquery-min-1-9.js' type='text/javascript'></script>
<script src='shoutbox_javascript.js' type='text/javascript'></script>
<?php $_SESSION['shout_banner'] = (!isset($_SESSION['shout_banner'])?"Main":$_SESSION['shout_banner']); ?>
<div class="shout_box">
<div class="header">Demo Shoutbox - <span class='cbanner'><?php echo $_SESSION['shout_banner']; ?></span><div class='all_btn'><span class="ascroll_btn">[A]</span> <span class="close_btn">[X]</span></div>
	<div class='bottom_header'>
	<hr style='margin-bottom: -4%; color: #1E90FF;'>
	<br><a href='#' class='room_link'>[Main]</a>  
			- <a href='#' class='room_link'>[Room2]</a> 
			- <a href='#' class='room_link'>[Room3]</a></div>
	</div>
  <div class="toggle_chat">
  <div class="message_box">
	</div>
	<div class="user_info">
		<input name="shout_message" id="shout_message" type="text" placeholder="Type Message Hit Enter" maxlength="100" />
	</div>
	</div>
</div>
</body>
</html>